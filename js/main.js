/**
 * ------------------------------------------------------------------- *
 * MAIN JS
 *
 * All of the javascript, logic, and many event handler in the document
 *
 * ------------------------------------------------------------------- *
 */

// handle the aos animation
// animate some element when scolling
AOS.init();
