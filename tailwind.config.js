const plugin = require("tailwindcss/plugin");

module.exports = {
  // Content needed
  content: [
    // define the development and source of content that place
    // the tailwind style component
    "./*.{html,js}",
  ],

  // Theme of the css,
  // can be customized based on the preference
  theme: {
    // font family
    // add some typograpgy and font family
    fontFamily: {
      sans: ["Manrope", "sans-serif"],
      writing: ["Pacifico", "cursive"],
    },
    extend: {
      // add some extends colors
      // so we can easily use them in our project
      colors: {
        "clean-grey": "#FAFBFF",
        "dark-blue-ocean": "#3734A9",
        "smooth-sky-blue": "#C9E7F2",
      },
    },
  },

  // The plugins needed
  // something like typography, and so on
  plugins: [
    // add some utilities for the css
    plugin(function ({ addUtilities }) {
      addUtilities({
        ".no-scrollbar": {
          "-ms-overflow-style": "none",
          "scrollbar-width": "none",
        },
        ".no-scrollbar::-webkit-scrollbar": {
          display: "none",
        },
      });
    }),
  ],
};
