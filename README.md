<div id="top"></div>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/35036127/icon.png" alt="Logo" width="100" height="100">
  </a>

  <br />
  <br />

  <h1 align="center"><b>Arson Landing Page With HTML</b></h1>

  <p align="center">
    Arson Landing page
    <br />
    <a href="https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html/">View Demo</a>
    ·
    <a href="https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html/-/issues">Request Feature</a>
  </p>
</div>

<br />
<br />

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#wiki">Wiki</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<br />

<!-- ABOUT THE PROJECT -->
<!-- all about the project, specify the background -->

## About The Project

<br />
<br />

<br />
<br />
Free landing page for use commercial purpose, we try to make and share what we can do and let the other, and world knowing and use what we built.
<br/>

#### **Appearance**

You can see the litle of appearance of amzing ui.

<br/>
<br/>

<br />
<br />
<br />

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

### Built With

This project build with some stack technology and service available, May there some service to updated. But for now this app work with amazing help from other stack, Here we go:

- [Remix.run](https://remix.run)
- [Figma](https://figma.com)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- GETTING STARTED -->
<!-- The way to start run this project into their device -->

## Getting Started

To start this project into your device workspace very easy. This may just tak a litle bit of your time to run this project, See the detail below.

### Prerequisites

There Library, and app you need before jumping to this project. Please install the needed prerequisites.

- **Remix Run**

  Remix is the whole and major behind of this project. They provide high excellent and amazing open source product that have better performance, and reliable. See the detail here [Documentation](https://remix.run/docs/en/v1)

### Installation

_Below is how you can run this poject with flutter as behind_

1. Clone this project, if you are trying to contribute consider to check the dev branch

   **Clone Using SSH**

   ```sh
   git clone git@gitlab.com:sonibble-creators/portfolios/meteo.git
   ```

   **Clone Using HTTPS**

   ```sh
   git clone https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html.git
   ```

2. Install the Dependencies
   ```sh
   npm install --silent
   ```
3. Run the project
   ```sh
    npm run dev
   ```

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- Wiki -->
<!-- enable the user to see the wiki of this project -->

## Wiki

Wwe build this project with some record of our documentation, If you interest to see the all about this project please check the wiki.

_For more detail, please refer to the [Wiki](https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html/-/wikis/home)_

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- ROADMAP -->
<!-- Initial info roadmap of this project -->

## Roadmap

- [x] Add Basic Feature
- [x] Add Animation
- [x] Add Documentation

See the [open issues](https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html/-/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Please check the contributing procedure [here](CONTRIBUTING.md), Don't forget to give the project a star! Thanks again!

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See [LICENSE](LICENSE.md) for more information.

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- CONTACT -->

## Contact

Nyoman Sunima - [@nyomansunima](https://instagram.com/nyomansunima) - nyomansunima@gmail.com

Sonibble - [@sonibble](https://instagram.com/sonibble) - [creative.sonibble@gmail.com](mailto:creative.sonibble@gmail.com)

Project Link: [https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html](https://gitlab.com/sonibble-creators/products/kits-templates/arson-landing-page/arson-landing-page-with-html)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>
